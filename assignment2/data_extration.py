import sys
import argparse
import os
import cv2
import pickle
import glob
import numpy as np
from math import floor
from sklearn.utils import shuffle

def unload_pickle(file):
    with open(file, "rb") as infile:
        X,Y = pickle.load(infile)
    return X,Y


def main(args):
    train_data, train_labels = unload_pickle(args.train_data)
    test_data, test_labels = unload_pickle(args.test_data)
    
    plate_files = glob.glob(args.new_data + "*/*_3.png")
    images = []
    for path in plate_files:
        img = cv2.imread(path)
        h,w,_ = img.shape
        n_height = floor(32*(h/w))
        img = cv2.resize(img, (32, n_height))
        white_pad = np.zeros((32-n_height,32,3), dtype=np.uint8)
        img = np.concatenate((img, white_pad))
        images.append(img)
    print("Gathered {} plate images".format(len(images)))
    images = np.array(images)
    images = shuffle(images)
    
    train_plates = images[:5000,:,:,:]
    train_plate_labels = 11*np.ones(5000)
    test_plates = images[5000:7000,:,:,:]
    test_plate_labels = 11*np.ones(2000)
    
    train_X = np.concatenate((train_data, train_plates))
    test_X = np.concatenate((test_data, test_plates))
    train_Y = np.concatenate((train_labels, train_plate_labels))
    test_Y = np.concatenate((test_labels, test_plate_labels))
    
    with open("train_data.pkl", "wb") as out:
        pickle.dump((train_X, train_Y), out)
    with open("test_data.pkl", "wb") as out:
        pickle.dump((test_X, test_Y), out)    

def arg_parser(argv):
    parser = argparse.ArgumentParser(description = "Merge the new data with the cifar-11 from assignment 1 and will also dumps out train_data.pkl and test_data.pkl")
    
    parser.add_argument("new_data", type=str,
               help="new data directory path")
    parser.add_argument("train_data", type=str,
               help="Train data pickle file")
    parser.add_argument("test_data", type=str,
               help="Test data pickle file")
        
    return parser.parse_args(argv)

if __name__ == "__main__":
    main(arg_parser(sys.argv[1:]))