##### Link to pre-extracted data
https://drive.google.com/open?id=15CzR0p-cpUejEQXWIAjsGDMdAIloPkKh

Có 2 loại data trong link, 1 loại ảnh lfw đã được cropped ra mặt, loại còn lại được giữ nguyên.
Dataset được crop mặt không có accuracy cao có lẽ vì chúng ít features hơn.
Trong notebook, model đạt chính xác khá ổn. Nếu tăng dropout lên 50% thì sẽ có khả năng test data accuracy sẽ cao hơn. Nhưng thời gian train sẽ rất lâu.
