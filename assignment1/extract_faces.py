import cv2
import os
import glob
import sys
import imghdr
import argparse

def main(args):
    CASCADE = args.cascade
    FACE_CASCADE = cv2.CascadeClassifier(CASCADE)
    image_paths = glob.glob(args.data_path+"*/*.jpg")
    if not "Extracted" in os.listdir("."):
        os.mkdir("Extracted")
    images = []
    i = 0
    for image_path in image_paths:
        image=cv2.imread(image_path)
        image_grey=cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)

        faces = FACE_CASCADE.detectMultiScale(image_grey,scaleFactor=1.16,minNeighbors=5,minSize=(25,25),flags=0)
        for x,y,w,h in faces:
            sub_img=image[y-10:y+h+10,x-10:x+w+10]
            cv2.imwrite("Extracted/"+str(i)+".jpg",sub_img)
            i += 1
            images.append(sub_img)
    print("Extracted faces: {}".format(i))
        
    extracted = glob.glob("./Extracted/*.jpg")
    removed = 0
    for file in extracted:
        if imghdr.what(file) == None:
            removed += 1
            os.remove(file) 
    print("Removed files: {}".format(removed))
    
def argument_parser(argv):
    parser = argparse.ArgumentParser(description="Extract faces from the dataset and store it in Extracted directory")
    
    parser.add_argument("cascade", type=str,
                        help="Path to the cascade file")
    parser.add_argument("data_path", type=str,
                        help="Path to the data")
    
    return parser.parse_args(argv)

if __name__ == "__main__":
    main(argument_parser(sys.argv[1:]))