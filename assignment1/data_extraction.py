import sys
import os
import argparse
import pickle
import cv2
import glob
from sklearn.model_selection import train_test_split
import numpy as np
import random

def get_data(data_path):
    data = np.zeros((len(data_path)*10000,32*32*3), dtype=np.uint8)
    labels = []
    for i in range(len(data_path)):
        with open(data_path[i], "rb") as file:
            cifar = pickle.load(file, encoding="bytes")
            start = i*10000
            end = start+10000
            data[start:end,:] = cifar[b"data"]
            labels += cifar[b"labels"]
    data = data.reshape((len(data_path)*10000,3,32,32))
    data = data.transpose(0,2,3,1)
    labels = np.array(labels, dtype=np.uint8)
    return data, labels

def main(args):
    # lfw
    n = 7000
    image_paths = glob.glob(args.pic_path + "*/*.jpg")
    images = []
    for i in image_paths:
        img = cv2.imread(i)
        img[:,:,:] = img[:,:,::-1]
        img = cv2.resize(img, (32,32))
        images.append(img)
    images_sample = [images[i] for i in random.sample(range(len(images)), n)]
    labels = np.ones(n,dtype=np.uint8)*10
    x_train, x_test, y_train, y_test = train_test_split(images_sample, labels, test_size=2000)
    x_train = np.array(x_train, dtype=np.uint8)
    x_test = np.array(x_test, dtype=np.uint8)
    print("Extracted lfw data")
          
    # cifar (this is under the assumption that test and train directories are seperate
    train_path = args.cifar_train
    test_path = args.cifar_test
    train_files = glob.glob(train_path+"*")
    test_files = glob.glob(test_path+"*")
    
    train_cifar, train_lbs = get_data(train_files)
    test_cifar, test_lbs = get_data(test_files)
    print("Extracted cifar data")
    print("Saving data")
    
    X_train = np.concatenate((train_cifar, x_train))
    Y_train = np.concatenate((train_lbs, y_train))
    X_test = np.concatenate((test_cifar, x_test))
    Y_test = np.concatenate((test_lbs, y_test))
    
    with open("train_data.pkl","wb") as out:
        pickle.dump((X_train, Y_train), out)
    with open("test_data.pkl","wb") as out:
        pickle.dump((X_test, Y_test), out)
    
def arg_parser(argv):
    parser = argparse.ArgumentParser(description="Taking the specified data paths and create a train_data and test_data pickle files")
    
    parser.add_argument("pic_path", type=str,
                        help="Data path for the lfw dataset")
    parser.add_argument("cifar_train" ,type=str,
                        help="Cifar's train data path")
    parser.add_argument("cifar_test", type=str,
                        help="Cifar's test data path")
    
    return parser.parse_args(argv)

if __name__=="__main__":
    main(arg_parser(sys.argv[1:]))
    

